﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="swimming.aspx.cs" Inherits="N01332758_assignment1.swimming" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Swimming lessons -- test</p>
            <div><asp:TextBox runat="server" ID="clientName" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            </div>
            <div>
                <asp:TextBox runat="server" ID="clientAge" placeholder="Age"></asp:TextBox>
                <asp:CompareValidator runat="server" ControlToValidate="clientAge" Type="String" Operator="GreaterThan" ValueToCompare="0" ErrorMessage="Age is invalid."></asp:CompareValidator>
            </div>
            <div><asp:TextBox runat="server" ID="ClientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a phone number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            </div>
            <div><asp:TextBox runat="server" ID="ClientEmail" placeholder="Email"></asp:TextBox>
             <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a valid email address" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            </div>
            <div><asp:DropDownList runat="server" ID="lessonTime">
                <asp:ListItem Value="9" Text="9:00"></asp:ListItem>
                <asp:ListItem Value="10" Text="10:00"></asp:ListItem>
                <asp:ListItem Value="11" Text="11:00"></asp:ListItem>
                <asp:ListItem Value="12" Text="12:00"></asp:ListItem>
                <asp:ListItem Value="1" Text="1:00"></asp:ListItem>
                <asp:ListItem Value="2" Text="2:00"></asp:ListItem>
                <asp:ListItem Value="3" Text="3:00"></asp:ListItem>
                <asp:ListItem Value="4" Text="4:00"></asp:ListItem>
                <asp:ListItem Value="5" Text="5:00"></asp:ListItem>
                <asp:ListItem Value="6" Text="6:00"></asp:ListItem>
            </asp:DropDownList></div>
            <div>
                <asp:RadioButton runat="server" Text="Private" GroupName="lessonType"/>
                <asp:RadioButton runat="server" Text="Group" GroupName="lessonType"/>
            </div><div>
                <asp:RadioButton runat="server" Text="Yellow Desert Pool" GroupName="facility"/>
                <asp:RadioButton runat="server" Text="Blue Mountain Pool" GroupName="facility"/>
                <asp:RadioButton runat="server" Text="Green Forest Pool" GroupName="facility"/>
            </div><div>
                 <h2>Day of week</h2>
                 <asp:CheckBox runat="server" ID="Mon" Text="Monday" />
                 <asp:CheckBox runat="server" ID="Tues" Text="Tuesday" />
                 <asp:CheckBox runat="server" ID="Wed" Text="Wednesday" />
                 <asp:CheckBox runat="server" ID="Thurs" Text="Thursday" />
                 <asp:CheckBox runat="server" ID="Fri" Text="Friday" />
            </div><div>
                 <h2>Medical conditions</h2>
                 <asp:CheckBox runat="server" ID="HeartDis" Text="Heart disease" />
                 <asp:CheckBox runat="server" ID="HardBreathing" Text="Hard of breathing" />
                 <asp:CheckBox runat="server" ID="Cancer" Text="Cancer" />
            </div><div>
                <br>
                 <asp:Button runat="server" ID="myButton" Text="Submit"/>
            </div>
        </div>
    </form>
</body>
</html>
